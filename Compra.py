#!/usr/bin/env pythoh3

'''
Programa para mostrar la lista de la compra
'''


def main():
    habitual = ("patatas", "leche", "pan")
    especifica = []

    while True:
        elemento = input("Elemento a comprar: ")
        if elemento == " ":
            break
        especifica.append(elemento)
        lista_compra = list(habitual) + especifica
        print("Lista de la compra: ")

        for i in lista_compra:
            print(i)

        elementos_habituales = len(habitual)
        elementos_especificos = len(especifica)
        elementos_lista = len(lista_compra)

        print("Elementos habituales: ", elementos_habituales)
        print("Elementos específicos: ", elementos_especificos)
        print("Elementos de la lista de la compra: ", elementos_lista)



if __name__ == '__main__':
    main()